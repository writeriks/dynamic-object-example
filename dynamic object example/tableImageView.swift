//
//  tableImageView.swift
//  dynamic object example
//
//  Created by Emir haktan Ozturk on 08/06/16.
//  Copyright © 2016 Emir haktan Ozturk. All rights reserved.
//

import UIKit

class tableImageView: UIImageView {

//    var lastLocation:CGPoint?
//    var panRecognizer:UIPanGestureRecognizer?
//    
//    init(imageIcon: UIImage?, location:CGPoint) {
//        super.init(image: imageIcon)
//        self.lastLocation = location
//        self.panRecognizer = UIPanGestureRecognizer(target:self, action:"detectPan:")
//        self.userInteractionEnabled = true
//        self.center = location
//        self.gestureRecognizers = [panRecognizer!]
//        self.frame = CGRect(x: location.x, y: location.y, width: 400.0, height: 50.0)
//    }
//    
//    required init?(coder aDecoder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
//    }
//    
//    func detectPan(recognizer:UIPanGestureRecognizer) {
//        let translation  = recognizer.translationInView(self.superview!)
//        self.center = CGPointMake(lastLocation!.x + translation.x, lastLocation!.y + translation.y)
//    }
//    
//    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
//        // Promote the touched view
//        self.superview?.bringSubviewToFront(self)
//        
//        // Remember original location
//        lastLocation = self.center
//    }

    var lastLocation:CGPoint?
    var prevPoint:CGPoint!
    
    var closeView : UIImageView!
    var resizeView: UIImageView!
    
    var panRecognizer:UIPanGestureRecognizer?
    var singleTap:UITapGestureRecognizer!
    var resizeTap:UIPanGestureRecognizer!
    var longPress:UILongPressGestureRecognizer!
    var closeButtons: UITapGestureRecognizer!
    var dummyGesture: UITapGestureRecognizer!
    
    init(imageIcon: UIImage?, location:CGPoint){
        super.init(image: imageIcon)
        self.lastLocation = location
        self.panRecognizer = UIPanGestureRecognizer(target:self, action:#selector(tableImageView.detectPan(_:)))
        self.longPress = UILongPressGestureRecognizer(target: self, action: #selector(tableImageView.longPress(_:)))
        self.dummyGesture = UITapGestureRecognizer(target: self, action: #selector(tableImageView.dummyAction(_:)))
        //self.closeButtons = UITapGestureRecognizer(target: self, action: #selector(tableImageView.closeButtons(_:)))
        
        self.userInteractionEnabled = true
        self.center = location
        self.gestureRecognizers = [panRecognizer!]
        self.addGestureRecognizer(longPress)
        self.addGestureRecognizer(dummyGesture)
        //self.addGestureRecognizer(closeButtons)
        
        self.frame = CGRect(x: location.x, y: location.y, width: 400.0, height: 50.0)
    }
    
    required init?(coder aDecoder: NSCoder) {
                fatalError("init(coder:) has not been implemented")
    }
    
    func dummyAction(recognizer:UITapGestureRecognizer){
        print("pressed image")
    }
    
    func detectPan(recognizer:UIPanGestureRecognizer) {
        let translation  = recognizer.translationInView(self.superview!)
        self.center = CGPointMake(lastLocation!.x + translation.x, lastLocation!.y + translation.y)
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        // Promote the touched view
        self.superview?.bringSubviewToFront(self)
        
        // Remember original location
        lastLocation = self.center
    }

    
    func longPress(recognizer:UILongPressGestureRecognizer){
        
        if(recognizer.state == UIGestureRecognizerState.Began){
            self.callButtons()
            
        }
    }
    
    /*
    
    func closeButtons(recognizer:UITapGestureRecognizer){
     
    }
    */
    
    func closeButtons2(){
        if self.closeView != nil && self.resizeView != nil{
        self.closeView.removeGestureRecognizer(singleTap)
        self.closeView.removeFromSuperview()
        self.closeView = nil
        
        self.resizeView.removeGestureRecognizer(resizeTap)
        self.resizeView.removeFromSuperview()
        self.closeView = nil
        }
    }
    
    
    func singleTap(recognizer:UITapGestureRecognizer) {
        let close = recognizer.view
        close?.superview?.removeFromSuperview()
    }
    
    func callButtons(){

        // Close Button
        self.closeView = UIImageView()
        self.closeView.image = UIImage(named: "closeButton.png")
        self.closeView.frame = CGRectMake(0, 0, 25, 25)
        self.closeView.backgroundColor = UIColor.clearColor()
        self.closeView.userInteractionEnabled = true
        
        self.singleTap = UITapGestureRecognizer(target: self, action: #selector(tableImageView.singleTap(_:)))
        self.closeView.addGestureRecognizer(self.singleTap!)
        
        self.addSubview(self.closeView)
        
        // Resize button right bottom corner
        self.resizeView = UIImageView()
        self.resizeView.image = UIImage(named: "resizeImage.png")
        self.resizeView.frame = CGRectMake(self.frame.size.width - 20, self.frame.size.height - 20, 25, 25)
        self.resizeView.backgroundColor = UIColor.clearColor()
        self.resizeView.userInteractionEnabled = true
        
        self.resizeTap = UIPanGestureRecognizer(target: self, action: #selector(tableImageView.resizeTap(_:)))
        self.resizeView.addGestureRecognizer(self.resizeTap!)
        
        self.addSubview(self.resizeView)
    }
    
    func resizeTap(recognizer:UIPanGestureRecognizer){
        
        if(recognizer.state == UIGestureRecognizerState.Began){
            prevPoint = recognizer.locationInView(self.superview)
            self.setNeedsDisplay()
        }
        else if(recognizer.state == UIGestureRecognizerState.Changed){
            
            if(self.bounds.size.width < 20){
                self.bounds = CGRectMake(self.bounds.origin.x, self.bounds.origin.y, 20, self.bounds.size.height)
                //self.redLocationA.frame = CGRectMake(12, 12, self.slfView.bounds.size.width - 24, self.slfView.bounds.size.height - 27)
                self.resizeView.frame = CGRectMake(self.bounds.size.width - 25, self.bounds.size.height - 25, 25, 25)
                self.closeView.frame = CGRectMake(0, 0, 25, 25)
            }
            
            if(self.bounds.size.height < 20){
                self.bounds = CGRectMake(self.bounds.origin.x, self.bounds.origin.y, self.bounds.size.width, 20)
                //self.redLocationA.frame = CGRectMake(12, 12, self.slfView.bounds.size.width - 24, self.slfView.bounds.size.height - 27)
                self.resizeView.frame = CGRectMake(self.bounds.size.width - 25, self.bounds.size.height - 25, 25, 25)
                self.closeView.frame = CGRectMake(0, 0, 25, 25)
            }
            
            let point:CGPoint = recognizer.locationInView(self.superview)
            var wChange:Float = 0.0
            var hChange:Float = 0.0
            
            wChange = (Float)(point.x - prevPoint.x)
            hChange = (Float)(point.y - prevPoint.y)
            
            
            self.bounds = CGRectMake(self.bounds.origin.x, self.bounds.origin.y, self.bounds.size.width + (CGFloat)(wChange), self.bounds.size.height + (CGFloat)(hChange))
            //self.redLocationA.frame = CGRectMake(12, 12, self.slfView.bounds.size.width - 24, self.slfView.bounds.size.height - 27)
            self.resizeView.frame = CGRectMake(self.bounds.size.width-25, self.bounds.size.height-25, 25, 25);
            self.closeView.frame = CGRectMake(0, 0, 25, 25)
            
            prevPoint = recognizer.locationInView(self.superview)
            
            self.setNeedsDisplay()
        }
            
        else if (recognizer.state == UIGestureRecognizerState.Ended){
            prevPoint = recognizer.locationInView(self.superview)
            self.setNeedsDisplay()
        }
    }


}
