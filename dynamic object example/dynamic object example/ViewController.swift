//
//  ViewController.swift
//  dynamic object example
//
//  Created by Emir haktan Ozturk on 03/06/16.
//  Copyright © 2016 Emir haktan Ozturk. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UINavigationControllerDelegate,UIImagePickerControllerDelegate,UIGestureRecognizerDelegate {
    
//    var slfView : UIView!
//    var closeView : UIImageView!
//    var resizeView: UIImageView!
//    
//    var singleTap:UITapGestureRecognizer!
//    var resizeTap:UIPanGestureRecognizer!
//    var longPress:UILongPressGestureRecognizer!
    
    var closeButtons: UITapGestureRecognizer!
    
    var prevPoint:CGPoint!
    
    var imagePicker: UIImagePickerController!
    var redLocationA:PinImageView!
    
    var redLocationB:tableImageView!
    //var redLocationA:PinImageView = PinImageView(imageIcon: UIImage(named: "stewie.jpg"), location: CGPointMake(30, 30))
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let button = UIButton(frame: CGRect(x: 100, y: 100, width: 100, height: 50))
        button.backgroundColor = .greenColor()
        button.setTitle("Test Button", forState: .Normal)
        button.addTarget(self, action: #selector(ViewController.callUI(_:)), forControlEvents: .TouchUpInside)
        
        self.view.addSubview(button)

    }
    
    func callUI(sender: UIButton!) {
        
        /*
         self.redLocationA = PinImageView(imageIcon: UIImage(named: "stewie.jpg"), location: CGPointMake(5, 30))
        self.view.addSubview(self.redLocationA)
        
        
        self.longPress = UILongPressGestureRecognizer(target: self, action: #selector(ViewController.longPress(_:)))
        self.redLocationA.addGestureRecognizer(longPress)
        
        */
 
        self.redLocationB = tableImageView(imageIcon: UIImage(named: "Orange.png"), location: CGPointMake(5, 30))
        
        self.view.addSubview(redLocationB)
        
        self.closeButtons = UITapGestureRecognizer(target: self, action: #selector(ViewController.closeButtons(_:)))
        closeButtons.delegate = self
        self.view.addGestureRecognizer(closeButtons)
        
    }
    /*
    func callButtons(){

        self.closeView = UIImageView()
        self.closeView.image = UIImage(named: "closeButton.jpeg")
        self.closeView.frame = CGRectMake(0, 0, 25, 25)
        self.closeView.backgroundColor = UIColor.clearColor()
        self.closeView.userInteractionEnabled = true
        
        self.singleTap = UITapGestureRecognizer(target: self, action: #selector(ViewController.singleTap(_:)))
        self.closeView.addGestureRecognizer(self.singleTap!)
        
        self.redLocationA.addSubview(self.closeView)
        
        // Resize button right bottom corner
        self.resizeView = UIImageView()
        self.resizeView.image = UIImage(named: "resizeImage.png")
        self.resizeView.frame = CGRectMake(self.redLocationA.frame.size.width - 20, self.redLocationA.frame.size.height - 20, 25, 25)
        self.resizeView.backgroundColor = UIColor.clearColor()
        self.resizeView.userInteractionEnabled = true
        
        self.resizeTap = UIPanGestureRecognizer(target: self, action: #selector(ViewController.resizeTap(_:)))
        self.resizeView.addGestureRecognizer(self.resizeTap!)
        
        self.redLocationA.addSubview(self.resizeView)
        

    }
 */
 
    /*
    func longPress(recognizer:UILongPressGestureRecognizer){

        if(recognizer.state == UIGestureRecognizerState.Began){
          self.callButtons()
            
        }
    }
    */
    
    
    func closeButtons(recognizer:UITapGestureRecognizer){
       /*
        self.closeView.removeGestureRecognizer(singleTap)
        self.closeView.removeFromSuperview()
        self.closeView = nil

        self.resizeView.removeGestureRecognizer(resizeTap)
        self.resizeView.removeFromSuperview()
        self.closeView = nil
         
        */
        
        redLocationB.closeButtons2()
    }
     
 
    
    /*
    func singleTap(recognizer:UITapGestureRecognizer) {
        let close = recognizer.view
        close?.superview?.removeFromSuperview()
    }
    */
    
    
    /*
    func resizeTap(recognizer:UIPanGestureRecognizer){

        if(recognizer.state == UIGestureRecognizerState.Began){
            prevPoint = recognizer.locationInView(self.redLocationA.superview)
            self.redLocationA.setNeedsDisplay()
        }
        else if(recognizer.state == UIGestureRecognizerState.Changed){
            
            if(self.redLocationA.bounds.size.width < 20){
                self.redLocationA.bounds = CGRectMake(self.redLocationA.bounds.origin.x, self.redLocationA.bounds.origin.y, 20, self.redLocationA.bounds.size.height)
                //self.redLocationA.frame = CGRectMake(12, 12, self.slfView.bounds.size.width - 24, self.slfView.bounds.size.height - 27)
                self.resizeView.frame = CGRectMake(self.redLocationA.bounds.size.width - 25, self.redLocationA.bounds.size.height - 25, 25, 25)
                self.closeView.frame = CGRectMake(0, 0, 25, 25)
            }
            
            if(self.redLocationA.bounds.size.height < 20){
                self.redLocationA.bounds = CGRectMake(self.redLocationA.bounds.origin.x, self.redLocationA.bounds.origin.y, self.redLocationA.bounds.size.width, 20)
                //self.redLocationA.frame = CGRectMake(12, 12, self.slfView.bounds.size.width - 24, self.slfView.bounds.size.height - 27)
                self.resizeView.frame = CGRectMake(self.redLocationA.bounds.size.width - 25, self.redLocationA.bounds.size.height - 25, 25, 25)
                self.closeView.frame = CGRectMake(0, 0, 25, 25)
            }

            let point:CGPoint = recognizer.locationInView(self.redLocationA.superview)
            var wChange:Float = 0.0
            var hChange:Float = 0.0
            
            wChange = (Float)(point.x - prevPoint.x)
            hChange = (Float)(point.y - prevPoint.y)
            
            
            self.redLocationA.bounds = CGRectMake(self.redLocationA.bounds.origin.x, self.redLocationA.bounds.origin.y, self.redLocationA.bounds.size.width + (CGFloat)(wChange), self.redLocationA.bounds.size.height + (CGFloat)(hChange))
            //self.redLocationA.frame = CGRectMake(12, 12, self.slfView.bounds.size.width - 24, self.slfView.bounds.size.height - 27)
            self.resizeView.frame = CGRectMake(self.redLocationA.bounds.size.width-25, self.redLocationA.bounds.size.height-25, 25, 25);
            self.closeView.frame = CGRectMake(0, 0, 25, 25)

            prevPoint = recognizer.locationInView(self.redLocationA.superview)
            
            self.redLocationA.setNeedsDisplay()
        }

        else if (recognizer.state == UIGestureRecognizerState.Ended){
            prevPoint = recognizer.locationInView(self.redLocationA.superview)
            self.redLocationA.setNeedsDisplay()
        }
    }
    
    */
}

