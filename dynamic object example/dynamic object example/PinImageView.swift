//
//  PinImageView.swift
//  dynamic object example
//
//  Created by Emir haktan Ozturk on 07/06/16.
//  Copyright © 2016 Emir haktan Ozturk. All rights reserved.
//

import UIKit

class PinImageView: UIImageView {
    
    var lastLocation:CGPoint?
    var panRecognizer:UIPanGestureRecognizer?
    
    init(imageIcon: UIImage?, location:CGPoint) {
        super.init(image: imageIcon)
        self.lastLocation = location
        self.panRecognizer = UIPanGestureRecognizer(target:self, action:#selector(PinImageView.detectPan(_:)))
        self.userInteractionEnabled = true
        self.center = location
        self.gestureRecognizers = [panRecognizer!]
        self.frame = CGRect(x: location.x, y: location.y, width: 400.0, height: 50.0)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func detectPan(recognizer:UIPanGestureRecognizer) {
        let translation  = recognizer.translationInView(self.superview!)
        self.center = CGPointMake(lastLocation!.x + translation.x, lastLocation!.y + translation.y)
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        // Promote the touched view
        self.superview?.bringSubviewToFront(self)
        
        // Remember original location
        lastLocation = self.center
    }

    
}
